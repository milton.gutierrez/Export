/*
 * To change this license datasColumns, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MG.exportexcel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author milton_gutierrez
 */
public class Tabla {

    private HashMap<String, Cell> datasColumns = new HashMap<String, Cell>();
    private HashMap<String, Cell> datasRows = new HashMap<String, Cell>();
    private ArrayList<String> columsHeader = new ArrayList<String>();
    private ArrayList<GroupsRows> groupsRows = new ArrayList<GroupsRows>();

    private int dataTableSizeX = 0;
    private int dataTableSizeY = 0;
    private int cellLengthX = 0;
    private Cell title = new Cell();
    private int indexColum = 0;
    private int startIndexX = 0;
    private boolean headerAutoBreak = Cell.NOT_AUTO_BREACK;
    private String headerAlign = Cell.HORIZONTAL_ALIGN;

    public int getStartIndexX() {
        return startIndexX;
    }

    public int getStartIndexY() {
        return startIndexY;
    }
    private int startIndexY = 0;

    private boolean existProperty(JSONObject jsonObj, String property) {
        return !jsonObj.isNull(property);
    }

    void printMap() {
        Iterator it;
        it = datasColumns.keySet().iterator();
        while (it.hasNext()) {
            Object key = it.next();
            System.out.println("Clave: " + key + " -> Valor: " + datasColumns.get(key).print());
        }
        System.out.println("*************************************************");
        it = datasRows.keySet().iterator();
        while (it.hasNext()) {
            Object key = it.next();
            System.out.println("" + key + " ->  " + datasRows.get(key).print());
        }
    }
    //}

    private String getRandomKey() {
        return UUID.randomUUID().toString();
    }

    private String getKeyOffByValue(HashMap<String, Cell> data, String value) {
        String result = "";
        Iterator it = data.keySet().iterator();
        while (it.hasNext()) {
            Object key = it.next();
            if (value.equals(data.get(key).getValue())) {
                result = (String) key;
            }
        }
        return result;
    }

    private void fillRowsEmpy() {
        int x = this.getMinX(datasColumns);
        int y = this.getMinY(datasColumns);

        int y2 = this.getMaxY(datasColumns);
        int x1 = this.getMaxX(datasColumns);

        int y1 = this.getMaxY(datasRows);

        for (int indexCol = x; indexCol < x1; indexCol++) { //Y

            for (int indexRows = y2; indexRows < y1; indexRows++) {
                String Key = indexCol + "_" + indexRows;
                if (datasRows.get((Object) Key) == null) {
                    datasRows.put(Key, new Cell("", indexCol, indexRows, 0, 1));
                }

            }
        }

    }

    void createCellRows(JSONObject jsonObj, int startIndexX, int startIndexY) {
        int gindexRows = 1;
        for (int g = 0; g < groupsRows.size(); g++) { //recoore rows by group
            JSONArray rowData = jsonObj.getJSONArray("rowData");
            for (int gindex = 0; gindex < groupsRows.get(g).getIndexPosition().size(); gindex++) { //recoore rows by group
                JSONObject data = rowData.getJSONObject(groupsRows.get(g).getIndexPosition().get(gindex));

                Iterator<?> keys = data.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    int indexColmun = columsHeader.indexOf(key);
                    if (indexColmun > -1) {
                        if (key.equals(this.nameFieldGroup)) {
                            String indexOf = this.getKeyOffByValue(datasRows, groupsRows.get(g).getLabelRows());// es igual a Distribucion
                            if (indexOf.length() > 0) {
                                datasRows.get(indexOf).setLengthY(datasRows.get(indexOf).getLengthY() + 1);
                            } else {
                                String Key = (startIndexX + indexColmun) + "_" + (startIndexY + gindexRows);
                                datasRows.put(Key, new Cell(data.get(key), startIndexX + indexColmun, startIndexY + gindexRows, 0, 1));
                            }
                        } else {
                            String Key = (startIndexX + indexColmun) + "_" + (startIndexY + gindexRows);
                            datasRows.put(Key, new Cell(data.get(key), startIndexX + indexColmun, startIndexY + gindexRows, 0, 1));
                        }
                    }
                }
                gindexRows++;
            }
        }
    }

    private void setDatasRows(JSONObject jsonObj, int startIndexX, int startIndexY) {
        try {
            if (this.nameFieldGroup.length() > 0) {
                this.getDistinctGroups(jsonObj);
                for (int g = 0; g < groupsRows.size(); g++) { //recoore rows by group
                    JSONArray rowData = jsonObj.getJSONArray("rowData");

                    for (int r = 0; r < rowData.length(); r++) {
                        JSONObject row = rowData.getJSONObject(r);
                        if (groupsRows.get(g).getLabelRows().equals(row.getString(this.nameFieldGroup))) { // x==json[r]
                            groupsRows.get(g).addIndexPosition(r);
                        }
                    }
                }
                this.createCellRows(jsonObj, startIndexX, startIndexY);
            } else {
                JSONArray rowData = jsonObj.getJSONArray("rowData");
                for (int indexRows = 0; indexRows < rowData.length(); indexRows++) {
                    JSONObject data = rowData.getJSONObject(indexRows);
                    Iterator<?> keys = data.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        int indexColmun = columsHeader.indexOf(key);
                        if (indexColmun > -1) {
                            String Key = (startIndexX + indexColmun) + "_" + (startIndexY + indexRows + 1);
                            Cell cell = new Cell(data.get(key), startIndexX + indexColmun, startIndexY + indexRows + 1, 0, 0);
                            datasRows.put(Key, cell);
                        }
                    }
                }
            }
            this.fillRowsEmpy();
        } catch (Exception ex) {
            System.out.println(ex);
        }

    }

    private int indexOfGroups(ArrayList<GroupsRows> groupsRows, String labelSearch) {
        int result = -1;
        for (int indexRows = 0; indexRows < groupsRows.size(); indexRows++) {
            if (groupsRows.get(indexRows).getLabelRows().equals(labelSearch)) {
                result = indexRows;
                continue;
            }
        }
        return result;

    }
    private String nameFieldGroup = "";

    void getDistinctGroups(JSONObject jsonObj) {
        this.groupsRows.clear();

        JSONArray rowData = jsonObj.getJSONArray("rowData");
        for (int i = 0; i < rowData.length(); ++i) {
            if (this.existProperty(rowData.getJSONObject(i), nameFieldGroup)) {
                int indexOf = indexOfGroups(this.groupsRows, rowData.getJSONObject(i).getString(nameFieldGroup));
                if (indexOf == -1) {
                    this.groupsRows.add(new GroupsRows(rowData.getJSONObject(i).getString(nameFieldGroup)));
                }
            }
        }
        for (int i = 0; i < this.groupsRows.size(); ++i) {
            System.err.println(this.groupsRows.get(i));
        }

    }
    private int cellLengthY = 0;

    private void setDatasHeader(int indexRows, JSONObject jsonObj, int startIndexX, int startIndexY) {
        try {

            dataTableSizeY = indexRows;
            Object valueCell = jsonObj.get("headerName");

            if (existProperty(jsonObj, "children")) {
                JSONArray childrenObjt = jsonObj.getJSONArray("children");
                cellLengthY++;
                String Key = (startIndexX + indexColum) + "_" + (startIndexY + indexRows);
                datasColumns.put(Key, new Cell(valueCell, startIndexX + indexColum, startIndexY + indexRows, childrenObjt.length(), cellLengthY));
                dataTableSizeX++;

                indexRows++;
                for (int i = 0; i < childrenObjt.length(); ++i) {
                    cellLengthY = 0;
                    JSONObject jsonObjChildren = childrenObjt.getJSONObject(i);
                    setDatasHeader(indexRows, jsonObjChildren, startIndexX, startIndexY);
                }
            } else {
                String key = (startIndexX + indexColum) + "_" + (startIndexY + indexRows);
                if (!jsonObj.getString("field").equals("actions")) {
                    columsHeader.add(jsonObj.getString("field"));
                    datasColumns.put(key, new Cell(valueCell, startIndexX + indexColum, startIndexY + indexRows, 0, 0, this.headerAlign, this.headerAutoBreak,true));
                    if (this.nameFieldGroup.length() == 0) {
                        if (existProperty(jsonObj, "rowGroupIndex")) {
                            this.nameFieldGroup = jsonObj.getString("field");
                        }
                    }
                    dataTableSizeX++;
                    indexColum++;
                    cellLengthX++;
                }
            }

        } catch (Exception ex) {
            System.out.println(ex);
        }

    }

    private String indexOffByValue(HashMap<String, Cell> data, Object value) {
        String result = "";
        Iterator it = data.keySet().iterator();
        while (it.hasNext()) {
            Object key = it.next();
            if (value.equals(datasColumns.get(key).getValue())) {
                result = (String) key;
            }
        }
        return result;
    }

    private void setdatasColumns(JSONObject jsonObj, int startIndexX, int startIndexY) {
        try {
            JSONArray columsJsonData = jsonObj.getJSONArray("columnDefs");
            for (int i = 0; i < columsJsonData.length(); ++i) {
                JSONObject jsonObjHeader = columsJsonData.getJSONObject(i);
                cellLengthX = 0;
                setDatasHeader(0, jsonObjHeader, startIndexX, startIndexY);
                String indexOf = indexOffByValue(datasColumns, jsonObjHeader.getString("headerName"));
                if (indexOf.length() > 0) {
                    if (existProperty(jsonObjHeader, "children")) {
                        datasColumns.get(indexOf).setLengthX(cellLengthX);
                    } else {
                        datasColumns.get(indexOf).setLengthX(0);
                    }
                    datasColumns.get(indexOf).setLengthY(cellLengthY);
                }

            }

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    void loadJSON(String jsonData) {
     

        loadJSON (jsonData, Cell.AUTO_BREACK, Cell.HORIZONTAL_ALIGN);

    }

    void loadJSON(String title, String jsonData) {

        loadJSON( jsonData, Cell.AUTO_BREACK, Cell.HORIZONTAL_ALIGN);

    }

    private int getMaxX(HashMap<String, Cell> datas) {
        int result = 0;
        Iterator it = datas.keySet().iterator();
        while (it.hasNext()) {
            Object key = it.next();
            if (result < datas.get(key).getPosX()) {
                result = datas.get(key).getPosX();
            }

        }
        return result + 1;
    }

    private int getMinX(HashMap<String, Cell> datas) {
        int result = 0;
        Iterator it = datas.keySet().iterator();
        while (it.hasNext()) {
            Object key = it.next();
            if (result > datas.get(key).getPosX()) {
                result = datas.get(key).getPosX();
            }

        }
        return result + 1;
    }

    private int getMinY(HashMap<String, Cell> datas) {
        int result = 0;
        Iterator it = datas.keySet().iterator();
        while (it.hasNext()) {
            Object key = it.next();
            if (result > datas.get(key).getPosY()) {
                result = datas.get(key).getPosY();
            }

        }
        return result + 1;
    }
    public int getTableMinX(){
        int result=getMinX( datasColumns);
        return result;
    }
    public int getTableMinY(){
        int result=getMinY( datasColumns);
        return result;
    }
    public int getTableMaxX(){
        int result=getMaxX(datasColumns);
        return result;
    }
    public int getTableMaxY(){
        int result=getMaxY(datasRows);
        return result;
    }
    private int getMaxY(HashMap<String, Cell> datas) {
        int result = 0;
        Iterator it = datas.keySet().iterator();
        while (it.hasNext()) {
            Object key = it.next();
            if (result < datas.get(key).getPosY()) {
                result = datas.get(key).getPosY();
            }

        }
        return result + 1;
    }

    private void updateHeigth() {
        int maxX = getMaxX(datasColumns);
        int maxY = getMaxY(datasColumns);
        Iterator it = datasColumns.keySet().iterator();
        while (it.hasNext()) {
            Object key = it.next();
            int indexY = datasColumns.get(key).getPosY();
            String nextKey = datasColumns.get(key).getPosX() + "_" + (indexY + 1);
            datasColumns.get(key).setLengthY(0);
            while (datasColumns.get(nextKey) == null && indexY < maxY) {
                datasColumns.get(key).setLengthY(datasColumns.get(key).getLengthY() + 1);
                indexY++;
                nextKey = datasColumns.get(key).getPosX() + "_" + (indexY);
            }

        }

    }

    void loadJSON( String jsonData,  boolean headerAutoBreak, String headerAlign) {

        this.headerAutoBreak = headerAutoBreak;
        this.headerAlign = headerAlign;

        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            setdatasColumns(jsonObject, 0, 0);
            startIndexY = getMaxY(datasColumns) - 1;
            setDatasRows(jsonObject, 0, startIndexY);
            updateHeigth();
            
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public HashMap<String, Cell> getDatasColumns() {
        return datasColumns;
    }

    public HashMap<String, Cell> getDatasRows() {
        return datasRows;
    }

    public ArrayList<String> getColumsHeader() {
        return columsHeader;
    }

    public int getDataTableSizeX() {
        return dataTableSizeX;
    }

    public int getDataTableSizeY() {
        return dataTableSizeY;
    }

    

    public int getSizeX() {
        return this.getColumsHeader().size();
    }

}
