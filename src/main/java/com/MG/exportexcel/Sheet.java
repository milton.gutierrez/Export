/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MG.exportexcel;

import java.awt.Color;
import java.awt.Dimension;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.HSSFRegionUtil;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFColor;

/**
 *
 * @author milton_gutierrez
 */
final class Sheet {

    Drawing patriarch = null;
    HSSFWorkbook workbook;
    HSSFSheet sheet;
    int startPosX=1;
    int startPosY=6;
    void addImage(Cell logo) {
        try {
            InputStream image = new FileInputStream(logo.getValueString());
            /* Convert Image to byte array */
            byte[] bytes = IOUtils.toByteArray(image);
            int picture_id = this.workbook.addPicture(bytes, this.workbook.PICTURE_TYPE_PNG);
            image.close();
            HSSFPatriarch drawing = this.sheet.createDrawingPatriarch();

            float columWidth = sheet.getColumnWidthInPixels(logo.getPosX());
            ClientAnchor anchor = new HSSFClientAnchor();
            anchor.setCol1(logo.getPosX()); //start X
            anchor.setRow1(logo.getPosY()); //start Y
            
            anchor.setCol2(logo.getPosX());   
            anchor.setRow2( logo.getPosY());
            HSSFPicture picture = drawing.createPicture(anchor, picture_id);

            picture.resize(logo.getLengthX(), logo.getLengthY());

        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public Sheet(String pathFile, Tabla tabla, Cell logoLeft, Cell LogoRigth,String title) {
        try {

            String filename = pathFile;
            this.workbook = new HSSFWorkbook();
            this.sheet = workbook.createSheet(title);
            sheet.getPrintSetup().setLandscape(true);
            sheet.getPrintSetup().setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
             //insert title
            Cell celTitle= new Cell(title, 0, -1, tabla.getColumsHeader().size(),1);
            createCell(sheet, getStylesDefaultTitle(sheet, celTitle), celTitle, true);
            
            Iterator it = tabla.getDatasColumns().keySet().iterator();
            while (it.hasNext()) {
                Object key = it.next();
                Cell celExcel = tabla.getDatasColumns().get(key);
                createCell(sheet, getStylesDefaultHeader(sheet, celExcel), celExcel, false);
            }
            it = tabla.getDatasRows().keySet().iterator();
            HSSFCellStyle style = getStylesDefault(sheet);
            while (it.hasNext()) {
                Object key = it.next();
                Cell celExcel = tabla.getDatasRows().get(key);

                createCell(sheet, style, celExcel, false);
            }

           
            FileOutputStream fileOut = new FileOutputStream(filename);

            //insert images
            addImage(logoLeft);
            addImage(LogoRigth);

            //add auto Size
            this.setSize(tabla);
            workbook.write(fileOut);
            fileOut.close();
            System.out.println("excel generate correctly!");

        } catch (Exception ex) {
            System.out.println(ex);
        }

    }

    private void setSize(Tabla tabla) {
        Iterator it = tabla.getDatasColumns().keySet().iterator();
        ArrayList<Integer> indexColumsHeader = new ArrayList<Integer>();
        while (it.hasNext()) {
            Object key = it.next();
            if (tabla.getDatasColumns().get(key).getTextAlign() == Cell.VERTICAL_ALIGN) {
                
                if(tabla.getDatasColumns().get(key).getValueString().length()<10){
                     
                     if(tabla.getDatasColumns().get(key).getValueString().equals("Region")){// TODO SPECIAL CASE
                         sheet.setColumnWidth(startPosX+tabla.getDatasColumns().get(key).getPosX(), 3250);
                     }else{
                         sheet.autoSizeColumn(startPosX+tabla.getDatasColumns().get(key).getPosX(), false);
                     }
                } else{
                     if(tabla.getDatasColumns().get(key).getValueString().equals("Region")){// TODO SPECIAL CASE
                         sheet.setColumnWidth(startPosX+tabla.getDatasColumns().get(key).getPosX(), 3250);
                     }else{
                         sheet.setColumnWidth(startPosX+ tabla.getDatasColumns().get(key).getPosX(), 2350);
                     }
                    
                }
                indexColumsHeader.add(startPosX+tabla.getDatasColumns().get(key).getPosX());
            } else {
                if(indexColumsHeader.indexOf(tabla.getDatasColumns().get(key).getPosX()+startPosX)>-1){
                    System.out.println(tabla.getDatasColumns().get(key).getPosX()+startPosX);
                }else{
                    sheet.autoSizeColumn(tabla.getDatasColumns().get(key).getPosX()+startPosX, true);
                }
                
            }

        }

    }
ArrayList<Integer> styleRows = new ArrayList<Integer>();
    private HSSFCellStyle getStylesDefaultHeader(HSSFSheet sheet, Cell celExcel) {
        HSSFCellStyle style = getStylesDefault(sheet);
        HSSFFont font = sheet.getWorkbook().createFont();
        style.setFillForegroundColor(HSSFColor.YELLOW.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        HSSFRow row = sheet.getRow(celExcel.getPosY()+startPosY);
        
        if (row == null) {
            row = sheet.createRow(celExcel.getPosY()+startPosY);
        }
        
        if (Cell.VERTICAL_ALIGN.equals(celExcel.getTextAlign()) && celExcel.isIsHeadColumn() )
        {
           
            row.setHeightInPoints(130);
            style.setRotation((short) 90);
           
        }
        if (Cell.VERTICAL_ALIGN.equals(celExcel.getTextAlign()) && celExcel.isIsHeadColumn() &&celExcel.getLengthY()>1 )
        {
            row.setHeightInPoints(50);
            style.setRotation((short) 90);
            
        }
        if (Cell.HORIZONTAL_ALIGN.equals(celExcel.getTextAlign()) && !celExcel.isIsHeadColumn() )
        {
            
            row.setHeightInPoints(50);
            style.setRotation((short) 0);
            
            
        }
         if (Cell.HORIZONTAL_ALIGN.equals(celExcel.getTextAlign()) && celExcel.isIsHeadColumn() )
        {
            styleRows.add(celExcel.getPosX());
            row.setHeightInPoints(60);
            style.setRotation((short) 0);
        }
       
        style.setFont(font);

        return style;
    }

    public HSSFCellStyle getStylesDefault(HSSFSheet sheet) {
        HSSFCellStyle style = sheet.getWorkbook().createCellStyle();
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setWrapText(true);
        return style;
    }
    private String  setAutobreack(String text, String textFind){
        String result="";
        if(text.length()>10){
            String[] auxResult= text.split(textFind);
            for (int i = 0; i < auxResult.length; i++) {
                
                if(auxResult[i].length()>2){
                result=result+auxResult[i]+"\n";
                  }else{
                  result=result+auxResult[i];  
                }
            }
        }     
        
        
        return result;
    }
    private void createCell(HSSFSheet sheet, HSSFCellStyle style, Cell celExcel, boolean isTitle) {

        HSSFRow row = sheet.getRow(celExcel.getPosY()+startPosY);
        if (row == null) {
            row = sheet.createRow(celExcel.getPosY()+startPosY);
        }
        HSSFCell cell = row.createCell(celExcel.getPosX()+startPosX);
        Object value = celExcel.getValue();
        //row.setRowStyle(getStyleRowsDefault(sheet));

        if (value instanceof String) {
            if (celExcel.isAutoBreack() && celExcel.getValueString().length()>10 ) {                
                value = setAutobreack(celExcel.getValueString()," ");
            }
            cell.setCellValue((String) value);
        }
        if (value instanceof Integer) {
            cell.setCellValue((int) value);
        }

        try {
            CellRangeAddress cellRangeAddress;
            if (!isTitle) {
                if (celExcel.getLengthX() > 1) { //concat cell in Rows
                    cellRangeAddress = new CellRangeAddress(celExcel.getPosY()+startPosY, celExcel.getPosY()+startPosY, (celExcel.getPosX()+startPosX), (startPosX+celExcel.getPosX() + celExcel.getLengthX() - 1));
                    HSSFRegionUtil.setBorderRight(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                    HSSFRegionUtil.setBorderLeft(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                    HSSFRegionUtil.setBorderBottom(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                    HSSFRegionUtil.setBorderTop(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                    sheet.addMergedRegion(cellRangeAddress);

                }
                if (celExcel.getLengthY() > 1) {// concat cell in columms

                    cellRangeAddress = new CellRangeAddress((celExcel.getPosY()+startPosY), (startPosY+celExcel.getPosY() + celExcel.getLengthY() - 1), celExcel.getPosX()+startPosX, celExcel.getPosX()+startPosX);
                    HSSFRegionUtil.setBorderBottom(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                    HSSFRegionUtil.setBorderRight(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                    HSSFRegionUtil.setBorderLeft(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                    HSSFRegionUtil.setBorderBottom(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                    HSSFRegionUtil.setBorderTop(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                    sheet.addMergedRegion(cellRangeAddress);

                }
            } else {
                cellRangeAddress = new CellRangeAddress(
                        celExcel.getPosY()+startPosY,
                        celExcel.getPosY()+startPosY,
                        celExcel.getPosX()+startPosX,
                        (celExcel.getPosX() + celExcel.getLengthX() - 1+startPosX)
                );
                HSSFRegionUtil.setBorderRight(CellStyle.BORDER_NONE, cellRangeAddress, sheet, workbook);
                HSSFRegionUtil.setBorderBottom(CellStyle.BORDER_NONE, cellRangeAddress, sheet, workbook);
                HSSFRegionUtil.setBorderLeft(CellStyle.BORDER_NONE, cellRangeAddress, sheet, workbook);
                HSSFRegionUtil.setBorderTop(CellStyle.BORDER_NONE, cellRangeAddress, sheet, workbook);
                sheet.addMergedRegion(cellRangeAddress);

            }
            cell.setCellStyle(style);

        } catch (Exception ex) {
            System.out.println(ex);
            }

    }

    private HSSFCellStyle getStylesDefaultTitle(HSSFSheet sheet, Cell celExcel) {

        HSSFRow row = sheet.getRow( this.startPosY + celExcel.getPosY());
        if (row == null) {
            row = sheet.createRow(this.startPosY +celExcel.getPosY());
        }
        HSSFCellStyle style = getStylesDefault(sheet);
        row.setHeight((short) 800);
        HSSFFont font = sheet.getWorkbook().createFont();
        font.setFontHeight((short) (16 * 20));
        font.setBold(true);
        style.setBorderBottom(CellStyle.BORDER_NONE);
        style.setBorderLeft(CellStyle.BORDER_NONE);
        style.setBorderRight(CellStyle.BORDER_NONE);
        style.setBorderTop(CellStyle.BORDER_NONE);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setFont(font);

        return style;

    }
}
