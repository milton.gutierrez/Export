/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MG.exportexcel;

import java.util.ArrayList;

/**
 *
 * @author milton_gutierrez
 */
public class GroupsRows {
    private String labelRows;
    private ArrayList<Integer> indexPosition;
    public GroupsRows(String labelRows) {
        this.labelRows = labelRows;
        this.indexPosition = new ArrayList<Integer>();
    }


    public String getLabelRows() {
        return labelRows;
    }

    public void setLabelRows(String labelRows) {
        this.labelRows = labelRows;
    }

    public ArrayList<Integer> getIndexPosition() {
        return indexPosition;
    }
    public void addIndexPosition(int index) {
        indexPosition.add(index);
    }

    public void setIndexPosition(ArrayList<Integer> indexPosition) {
        this.indexPosition = indexPosition;
    }
    
}
