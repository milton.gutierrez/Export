/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MG.exportexcel;

/**
 *
 * @author Milton Gutierrez
 */
public class ReportsExcel extends Reports{

    public ReportsExcel(String logoLeft, String LogoRigth) {
        super.LogoLeft = logoLeft;
        super.LogoRigth = LogoRigth;
    }

    public ReportsExcel() {
    }

    private void exportReportExcel(String title, String JSONReporte, int imageWith, int imageHeigth,int imageWith1, int imageHeigth1, boolean headAutoBreak, String headAlign) {
        if (JSONReporte.length() > 0) {
            Tabla tabla = new Tabla();
            tabla.loadJSON( JSONReporte,  headAutoBreak, headAlign);
            tabla.printMap();
            int posX = super.posisionInicialX;
            int PosY = 2;
            Cell cellLogoLeft = new Cell(super.LogoLeft, posX, PosY, imageWith, imageHeigth);
            posX = (super.posisionInicialX + tabla.getSizeX()) - imageWith1;
            Cell cellLogoRigth = new Cell(super.LogoRigth, posX, PosY, imageWith1, imageHeigth1);
             Sheet exportExcel = new Sheet(title + ".xls", tabla, cellLogoLeft, cellLogoRigth,title);
        }
    }

    private void exportReportExcel(String title, String JSONReporte, int imageWith, int imageHeigth) {
        boolean headAutoBerak = Cell.AUTO_BREACK;
        String  headAlign = Cell.HORIZONTAL_ALIGN;
        exportReportExcel(title, JSONReporte, imageWith, imageHeigth, imageWith, imageHeigth,headAutoBerak, headAlign);
    }

    public void ListadoDeEquiposInstalados(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 3;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.LISTADO_DE_EQUIPOS_INSTALADOS, report, imageWith, imageHeigth);
    }

    public void FallasRegistradas(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 3;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.FALLAS_REGISTRADAS, report, imageWith, imageHeigth);
    }

    public void ResumenDeFallasRegistradas(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 4;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.RESUMEN_DE_FALLAS_REGISTRADAS, report, imageWith, imageHeigth);

    }

    public void IndisponibilidadDeEquipos(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 3;
        int imageHeigth = 1;
        reporte.exportReportExcel(super.INDISPONIBILIDAD_DE_EQUIPOS, report, imageWith, imageHeigth);
    }

    public void ProgramaDeMantenimiento(String report) {
        ReportsExcel reporte = new ReportsExcel();
        //logo Rigth
        int imageWith = 3;
        int imageHeigth = 4;
        //logo Rigth
        int imageWith1 = 6;
        int imageHeigth1 = 4;
        reporte.exportReportExcel(super.PROGRAMA_DE_MANTENIMIENTO, report, imageWith, imageHeigth,imageWith1, imageHeigth1,Cell.AUTO_BREACK,Cell.HORIZONTAL_ALIGN);
    }

    public void ProgramaDeDespeje(String report) {
        ReportsExcel reporte = new ReportsExcel();
        //logo left
        int imageWith = 3;
        int imageHeigth = 4;
        //logo Rigth
        int imageWith1 = 6;
        int imageHeigth1 = 4;
        reporte.exportReportExcel(super.PROGRAMA_DE_DESPEJE, report, imageWith, imageHeigth,imageWith1, imageHeigth1,Cell.AUTO_BREACK,Cell.HORIZONTAL_ALIGN);
    }

    public void EvaluaciónDeProgramasDeMantenimiento(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 2;
        int imageHeigth = 1;
        reporte.exportReportExcel(super.EVALUACION_DE_PROGRAMAS_DE_MANTENIEMIENTO, report, imageWith, imageHeigth);
    }

    public void MantenimientoConCambioDeSilica(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 2;
        int imageHeigth = 1;
        reporte.exportReportExcel(super.MANTENIMIENTO_CON_CAMBIO_DE_SILICA, report, imageWith, imageHeigth);
    }

    public void AntiguedadDeTransformadores(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 3;
        int imageHeigth = 4;
         int imageWith1 = 2;
        int imageHeigth1 = 4;
        reporte.exportReportExcel(super.ANTIGUEDAD_DE_TRANFORMADORES, report, imageWith, imageHeigth,imageWith1, imageHeigth1,Cell.AUTO_BREACK,Cell.HORIZONTAL_ALIGN);
    }

    public void MantenimientoASistemasDeMando(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 2;
        int imageHeigth = 4;
        int imageWith1 = 1;
        int imageHeigth1 = 4;
        reporte.exportReportExcel(super.MANTENIMIENTO_A_SISTEMAS_DE_MANDO, report, imageWith, imageHeigth,imageWith1, imageHeigth1,Cell.AUTO_BREACK,Cell.HORIZONTAL_ALIGN);
    }

    public void MantenimientoASistemasDeEnfriamiento(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 2;
        int imageHeigth = 4;
        int imageWith1 = 1;
        int imageHeigth1 = 4;
        reporte.exportReportExcel(super.MANTENIMIENTO_A_SISTEMAS_DE_ENFRIAMIENTO, report, imageWith, imageHeigth,imageWith1, imageHeigth1,Cell.AUTO_BREACK,Cell.HORIZONTAL_ALIGN);
    }

    public void DespejesEjecutados(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 1;
        int imageHeigth = 4;
        int imageWith1 = 4;
        int imageHeigth1 = 4;
        reporte.exportReportExcel(super.DESPEJES_EJECUTADOS, report, imageWith, imageHeigth,imageWith1, imageHeigth1,Cell.AUTO_BREACK,Cell.HORIZONTAL_ALIGN);
    }

    public void MantenimientoACambiadores(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 3;
        int imageHeigth = 1;
        reporte.exportReportExcel(super.MANTENIMIENTO_A_CAMBIADORES, report, imageWith, imageHeigth);
    }

    public void equiposdeSubEstacionInstalados(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 3;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.EQUIPOS_DE_SUBESTACIÓN_INSTALADOS, report, imageWith, imageHeigth);
    }

    public void interrupcionesPropiasDelEquipo(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 3;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.INTERRUPCIONES_PROPIAS_DEL_EQUIPO, report, imageWith, imageHeigth);
    }

    public void interrupcionesAjenasAlEquipo(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 3;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.INTERRUPCIONES_AJENAS_AL_EQUIPO, report, imageWith, imageHeigth);
    }

    public void evaluacionDeMantenimientoDeInterruptores(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 2;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.EVALUACIÓN_DE_MANTENIMIENTO_DE_INTERRUPTORES, report, imageWith, imageHeigth);
    }

    public void evaluacionDeMantenimientoDeSeccionadores(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 2;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.EVALUACION_DE_MANTENIMIENTO_DE_SECCIONADORES, report, imageWith, imageHeigth);
    }

    public void evaluacionDeMantenimientoDeTransofrmadoresDePotencia(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 2;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.EVALUACIÓN_DE_MANTENIMIENTO_DE_TRANSFORMADORES_DE_POTENCIA, report, imageWith, imageHeigth);
    }

    public void evaluacionDeMantenimientoEnTransofrmadoresDeCorriente(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 2;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.EVALUACIÓN_DE_MANTENIMIENTO_EN_TRANSFORMADORES_DE_CORRIENTE, report, imageWith, imageHeigth);
    }

    public void evaluacionDeMantenimientoDeApartaRayos(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 2;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.EVALUACIÓN_DE_MANTENIMIENTO_DE_APARTARRAYOS, report, imageWith, imageHeigth);
    }

    public void evaluacionDeProgramaDeManteniemientoXEquipo(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 3;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.EVALUACIÓN_DE_PROGRAMA_DE_MANTENIMIENTO_X_EQUIPO, report, imageWith, imageHeigth);
    }

    public void evaluacionDeProgramaDeDespejeXBahiaAtendida(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 2;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.EVALUACIÓN_DE_PROGRAMA_DE_DESPEJE_X_BAHÍA_ATENDIDA, report, imageWith, imageHeigth);
    }

    public void programaDeMantenimientoEnSubEstaciones(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 5;
        int imageHeigth = 4;
        int imageWith1 = 5;
        int imageHeigth1 = 4;
        reporte.exportReportExcel(super.PROGRAMA_DE_MANTENIMIENTO_EN_SUBESTACIONES, report, imageWith, imageHeigth,imageWith1, imageHeigth1,Cell.AUTO_BREACK,Cell.HORIZONTAL_ALIGN);
    }

    public void programaDeMantenimientoEnVeranoDeSubEstacion(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 3;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.PROGRAMA_DE_MANTENIMIENTO_EN_VERANO_DE_SUBESTACIÓN, report, imageWith, imageHeigth);
    }

    public void programaDeMantenimientoEnInviernoDeSubEstacion(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 3;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.PROGRAMA_DE_MANTENIMIENTO_EN_INVIERNO_DE_SUBESTACIONES, report, imageWith, imageHeigth);
    }

    public void programaDeDespejeSubEstaciones(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 3;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.PROGRAMA_DE_DESPEJE_SUBESTACIONES, report, imageWith, imageHeigth);
    }

    public void programaDeDespejeEnVerano(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 3;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.PROGRAMA_DE_DESPEJE_EN_VERANO, report, imageWith, imageHeigth);
    }

    public void programaDeDespejeEnInvierno(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 3;
        int imageHeigth = 4;
        reporte.exportReportExcel(super.PROGRAMA_DE_DESPEJE_EN_INVIERNO, report, imageWith, imageHeigth);
    }

    public void despejeSE(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 6;
        int imageHeigth = 4;
         int imageWith1 = 6;
        int imageHeigth1 = 4;
        reporte.exportReportExcel(super.DESPEJE_SE, report, imageWith, imageHeigth,imageWith1, imageHeigth1,Cell.NOT_AUTO_BREACK,Cell.VERTICAL_ALIGN);
    }

    public void despejeTF(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 6;
        int imageHeigth = 4;
         int imageWith1 = 6;
        int imageHeigth1 = 4;
        reporte.exportReportExcel(super.DESPEJE_TF, report, imageWith, imageHeigth,imageWith1, imageHeigth1,Cell.NOT_AUTO_BREACK,Cell.VERTICAL_ALIGN);
    }

    public void mantenimientoSalaDeGRuas(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 6;
        int imageHeigth = 4;
         int imageWith1 = 6;
        int imageHeigth1 = 4;
        reporte.exportReportExcel(super.MANTENIEMIENTO_SALA_DE_GRUAS, report, imageWith, imageHeigth,imageWith1,imageHeigth1,Cell.NOT_AUTO_BREACK,Cell.VERTICAL_ALIGN);
    }

    public void mantenientoSE(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 5;
        int imageHeigth = 4;
         int imageWith1 = 8;
        int imageHeigth1 = 4;
        reporte.exportReportExcel(super.MANTENIMIENTO_SE, report, imageWith, imageHeigth, imageWith1,imageHeigth1,Cell.NOT_AUTO_BREACK,Cell.VERTICAL_ALIGN);
    }

    public void soportetecnicoTF(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 6;
        int imageHeigth = 4;
         int imageWith1 = 10;
        int imageHeigth1 = 4;
        reporte.exportReportExcel(super.SOPORTE_TECNICO_TF, report, imageWith, imageHeigth,imageWith1,imageHeigth1,Cell.NOT_AUTO_BREACK,Cell.VERTICAL_ALIGN);
    }

    public void trasladoYMontaje(String report) {
        ReportsExcel reporte = new ReportsExcel();
        int imageWith = 2;
        int imageHeigth = 4;
         int imageWith1 = 6;
        int imageHeigth1 = 4;
        reporte.exportReportExcel(super.TRASLADO_Y_MONTAJE, report, imageWith, imageHeigth,imageWith1,imageHeigth1,Cell.NOT_AUTO_BREACK,Cell.VERTICAL_ALIGN);
    }
    
    
    
   
    

}
