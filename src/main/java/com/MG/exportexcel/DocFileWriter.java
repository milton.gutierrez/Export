package com.MG.exportexcel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlException;
import static org.openxmlformats.schemas.drawingml.x2006.main.STPenAlignment.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDocument1;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageSz;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STPageOrientation;

public class DocFileWriter {

    String title;
    String description;
    //Blank Document
    XWPFDocument document;
    String pathFile;
    FileOutputStream out;

    public void createHeader() throws IOException, XmlException {
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        XWPFHeaderFooterPolicy policy = new XWPFHeaderFooterPolicy(document, sectPr);
        //write header content
        CTP ctpHeader = CTP.Factory.newInstance();
        CTR ctrHeader = ctpHeader.addNewR();
        CTText ctHeader = ctrHeader.addNewT();
        String headerText = "This is header";

        ctHeader.setStringValue(headerText);
        XWPFParagraph headerParagraph = new XWPFParagraph(ctpHeader, document);
        XWPFParagraph[] parsHeader = new XWPFParagraph[1];
        parsHeader[0] = headerParagraph;
        policy.createHeader(XWPFHeaderFooterPolicy.DEFAULT, parsHeader);
    }

    public void createFooter() throws IOException, XmlException {
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        XWPFHeaderFooterPolicy policy = new XWPFHeaderFooterPolicy(document, sectPr);
        //write footer content
        CTP ctpFooter = CTP.Factory.newInstance();
        CTR ctrFooter = ctpFooter.addNewR();
        CTText ctFooter = ctrFooter.addNewT();
        String footerText = "This is footer";
        ctFooter.setStringValue(footerText);
        XWPFParagraph footerParagraph = new XWPFParagraph(ctpFooter, document);
        XWPFParagraph[] parsFooter = new XWPFParagraph[1];
        parsFooter[0] = footerParagraph;
        policy.createFooter(XWPFHeaderFooterPolicy.DEFAULT, parsFooter);
    }

    public void AddImage(String imgFile) {
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun paragraphOneRunOne = paragraph.createRun();
        AddImage(imgFile, paragraphOneRunOne);
    }

    public void AddImage(String imgFile, XWPFRun run) {

        FileInputStream is;
        try {
            is = new FileInputStream(imgFile);
            run.addBreak();
            run.addPicture(is, XWPFDocument.PICTURE_TYPE_PNG, imgFile, Units.toEMU(200), Units.toEMU(200)); // 200x200 pixels
            is.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DocFileWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DocFileWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidFormatException ex) {
            Logger.getLogger(DocFileWriter.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void addTitle() {
        //Set Bold an Italic
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun paragraphOneRunOne = paragraph.createRun();
        paragraphOneRunOne.setBold(true);
        paragraphOneRunOne.setItalic(true);
        paragraphOneRunOne.setText(this.title);
        
        paragraph.setAlignment(ParagraphAlignment.CENTER);
        paragraphOneRunOne.addBreak();
    }

    public void addParagraph(String text) {
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.setText(text);
    }

    public void addTable(String JSONData) {
        Tabla tabla = new Tabla();
        tabla.loadJSON(JSONData);
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun paragraphOneRunOne = paragraph.createRun();
        paragraphOneRunOne.addBreak(BreakType.PAGE);
        /**
         * *****vertical page aligne***
         */
        CTDocument1 document = this.document.getDocument();
        CTBody body = document.getBody();
        if (!body.isSetSectPr()) {
            body.addNewSectPr();
        }
        CTSectPr section = body.getSectPr();
        if (!section.isSetPgSz()) {
            section.addNewPgSz();
        }
        CTPageSz pageSize = section.getPgSz();

        //pageSize.setOrient(STPageOrientation.LANDSCAPE);
        pageSize.setW(BigInteger.valueOf(15840));
        pageSize.setH(BigInteger.valueOf(12240));
        /**
         * **************** create Table***************
         */
        XWPFTable table = this.document.createTable(tabla.getTableMaxY(), tabla.getTableMaxX());
        for (int colIndex = tabla.getTableMinX() - 1; colIndex <= tabla.getTableMaxX() - 1; colIndex++) {
            for (int rowIndex = tabla.getTableMinY() - 1; rowIndex <= tabla.getTableMaxY() - 1; rowIndex++) {
                Cell celExcel = tabla.getDatasColumns().get(colIndex + "_" + rowIndex);
                String value = "";
                int lengthY = 0;
                int lengthX = 0;
                String textAligned = Cell.HORIZONTAL_ALIGN;
                if (celExcel != null) {
                    value = celExcel.getValueString();
                    lengthY = celExcel.getLengthY();
                    lengthX = celExcel.getLengthX();
                    textAligned = celExcel.getTextAlign();
                } else {
                    celExcel = tabla.getDatasRows().get(colIndex + "_" + rowIndex);
                    if (celExcel != null) {
                        value = celExcel.getValueString();
                        lengthY = celExcel.getLengthY();
                        lengthX = celExcel.getLengthX();
                        textAligned = celExcel.getTextAlign();
                    }
                }
                XWPFTableRow tableRowOne = table.getRow(rowIndex);
                tableRowOne.getCell(colIndex).setText(value);
                tableRowOne.getCell(colIndex).setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                //if(textAligned.equals(Cell.VERTICAL_ALIGN)){
                   /* System.out.println(value);
                    System.out.println(value);
                    System.out.println(lengthY);*/
                if (lengthY > 1) {
                    mergeTableCellsVertically(table, colIndex, rowIndex, rowIndex + lengthY-1);
                }
                if (lengthX > 1) {
                    //if (value.equals("Sistemas en Mantenimiento")) {
                        
                        mergeTableCellsHorizontally(table, rowIndex, colIndex, colIndex+ lengthX-1);
                   // }
                }
            }

        }

    }

    private void mergeTableCellsVertically(XWPFTable table,
            int col, int fromRow, int toRow) {
        for (int rowIndex = fromRow; rowIndex <= toRow; rowIndex++) {
            XWPFTableCell cell = table.getRow(rowIndex).getCell(col);
            if (rowIndex == fromRow) {
                // The first merged cell is set with RESTART merge value
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.RESTART);
            } else {
                // Cells which join (merge) the first one, are set with CONTINUE
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.CONTINUE);
            }
        }
    }

    private void mergeTableCellsHorizontally(XWPFTable table,
            int row, int fromCol, int toCol) {
        for (int colIndex = fromCol; colIndex <= toCol; colIndex++) {
            XWPFTableCell cell = table.getRow(row).getCell(colIndex);
            if (colIndex == fromCol) {
                // The first merged cell is set with RESTART merge value
                cell.getCTTc().addNewTcPr().addNewHMerge().setVal(STMerge.RESTART);
            } else {
                // Cells which join (merge) the first one, are set with CONTINUE
                cell.getCTTc().addNewTcPr().addNewHMerge().setVal(STMerge.CONTINUE);
            }
        }
    }

    public DocFileWriter(String pathFile, String title, String description, String jsontable) {
        this.title = title;
        this.description = description;
        this.pathFile = pathFile;
        this.document = new XWPFDocument();
        try {
            createHeader();
            addTitle();
            addParagraph(description);
            addTable(jsontable);
            createFooter();
            this.out = new FileOutputStream(
                    new File(pathFile));

        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public void createDoc() {
        try {
            document.write(this.out);
            out.close();
            System.out.println("createdocument.docx written successully");
        } catch (IOException ex) {
            Logger.getLogger(DocFileWriter.class.getName()).log(Level.SEVERE, null, ex);

        }
    }
}
