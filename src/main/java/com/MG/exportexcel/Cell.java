/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.MG.exportexcel;

import sun.security.util.Length;

/**
 *
 * @author milton_gutierrez
 */
public class Cell {
    public static String VERTICAL_ALIGN ="VERTICAL";
    public static String HORIZONTAL_ALIGN ="HORIZONTAL";
    public static boolean AUTO_BREACK = true;
    public static boolean NOT_AUTO_BREACK = false;
    private Object value;
    private int posX = 0;
    private int posY = 0;
    private int lengthX = 1;
    private int lengthY = 1;
    private int with = 1;
    private String textAlign=HORIZONTAL_ALIGN;
    private boolean autoBreack=NOT_AUTO_BREACK;
    private boolean isHeadColumn=false;

    public void setLengthY(int lengthY) {
        this.lengthY = lengthY;
    }

    public Object getValue() {
        return value;
    }
    public String getValueString() {
        return value.toString();
    }
    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public int getLengthX() {
        return lengthX;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public void setLengthX(int length) {
        this.lengthX = length;
    }

    public Cell() {
        this.value = new String();
        this.posX = 0;
        this.posY = 0;
        this.lengthX = 1;
        this.lengthY = 1;

    }

    public String print() {
        return "label : " + value + "\n PosX : " + posX + "\n PosY : " + posY + "\n lengthX : " + lengthX + "\n lengthY : " + lengthY +"\ntextAlign : "+textAlign+"\nautoBreack : "+autoBreack;
    }

    public Cell(String value) {
        this.value = value;
        this.posX = 0;
        this.posY = 0;
        this.lengthX = 1;
        this.lengthY = 1;
        this.textAlign=HORIZONTAL_ALIGN;
        this.autoBreack=NOT_AUTO_BREACK;

    }
 public Cell(Object label, int posX, int posY, int lengthX, int lengthY) {
        this.value = label;
        this.posX = posX;
        this.posY = posY;
        this.lengthX = lengthX;
        this.textAlign=HORIZONTAL_ALIGN;
        this.autoBreack=NOT_AUTO_BREACK;
    }
    public Cell(Object label, int posX, int posY, int lengthX, int lengthY, String textAlign,boolean autoBreak, boolean isHeadColumn) {
        this.value = label;
        this.posX = posX;
        this.posY = posY;
        this.lengthX = lengthX;
        this.lengthY = lengthY;
        this.textAlign =textAlign;
        this.autoBreack=autoBreak;
        this.isHeadColumn= isHeadColumn;

    }
    public Cell(String label, int posX, int posY, int lengthX, int lengthY) {
        this.value = (String)label;
        this.posX = posX;
        this.posY = posY;
        this.lengthX = lengthX;
        this.lengthY = lengthY;

    }

    public int getLengthY() {
        return lengthY;
    }

    void setCellValue(String namE2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getTextAlign() {
        return textAlign;
    }

    public void setTextAlign(String textAlign) {
        this.textAlign = textAlign;
    }

    public boolean isAutoBreack() {
        return autoBreack;
    }

    public void setAutoBreack(boolean autoBreack) {
        this.autoBreack = autoBreack;
    }

    public boolean isIsHeadColumn() {
        return isHeadColumn;
    }

    public void setIsHeadColumn(boolean isHeadColumn) {
        this.isHeadColumn = isHeadColumn;
    }
     

}
